#!/bin/sh
#!/usr/bin/env bash
# Note: Matrix runs the build script from BASEDIR/MATRIX where BASEDIR is the root dir of the git project.
# if this changes, the script will break.
BASEDIR=. #changed but may change again depending on matrix-path-policy
cd $BASEDIR
mvn clean package -DskipTests
