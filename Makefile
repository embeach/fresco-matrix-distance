build: install move

install:
	mvn clean install -DskipTests

move:
	mkdir -p server1;
	mkdir -p server2;
	cp target/matrix-distance.jar server1;
	cp target/matrix-distance.jar server2;

runSpdz:
	cd server1 && java -jar matrix-distance.jar -e SEQUENTIAL_BATCHED -i 2 -p 1:localhost:8081 -p 2:localhost:8082 -s spdz -Dspdz.preprocessingStrategy=DUMMY > log.txt 2>&1 &
	cd server2 && java -jar matrix-distance.jar -e SEQUENTIAL_BATCHED -i 1 -p 1:localhost:8081 -p 2:localhost:8082 -s spdz -Dspdz.preprocessingStrategy=DUMMY  2>&1 | tee log.txt
